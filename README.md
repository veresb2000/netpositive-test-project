to run the project:
 - cd netpositive-test-project
 - composer install
 - php artisan migrate --seed
 - php artisan serve
 - localhost:8000/list