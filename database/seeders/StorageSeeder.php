<?php

namespace Database\Seeders;

use App\Models\Storage;
use Illuminate\Database\Seeder;

class StorageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $inputs = config('storage.input');
        array_shift($inputs);
        foreach($inputs as $input)
        {
            $array = preg_split('/\,/', $input);
                Storage::create([
                    'id' => $array[0],
                    'title' => $array[1],
                    'level' => $array[2],
                    'net' => $array[3],
                    'count' => $array[4],
                    'vat' => $array[5],
                ]);
        }
    }
}
