<style>
    table,
    th,
    td {
        border: 1px solid;
    }

    .number {
        text-align: right;
    }
</style>

<div class="card-body">
    <div class="row">
        <div class="col">
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th>title</th>
                            <th>level</th>
                            <th>net</th>
                            <th>count</th>
                            <th>vat</th>
                            <th>sum gross value</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($storages as $storage)
                            <tr>
                                <td>{{ $storage->title }}</td>
                                <td class="number"><a href="{{route('list', ['level' => $storage->level])}}">{{ $storage->level }}</a></td>
                                <td class="number">{{ $storage->net }}</td>
                                <td class="number">{{ $storage->count }}</td>
                                <td class="number">{{ $storage->vat . '%' }}</td>
                                <td class="number">{{ $storage->count*($storage->net+($storage->vat/100)) }}</td>
                            </tr>
                        @endforeach
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td class="number">{{$sumGrossValueCount}}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
