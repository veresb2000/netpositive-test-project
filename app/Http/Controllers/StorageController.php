<?php

namespace App\Http\Controllers;

use App\Models\Storage;
use Illuminate\Http\Request;

class StorageController extends Controller
{
    public function list(Request $request)
    {
        $storages = $this->getList();
        $sumGrossValueCount = 0;
        foreach($storages as $storage)
        {
            $sumGrossValueCount += $storage->count*($storage->net+($storage->vat/100));
        }
        return view('storage', compact('storages', 'sumGrossValueCount'));
    }

    private function getList()
    {
        $level = request()->route('level');
        $storages = $level ? Storage::where('level', '>=', $level)->get() : Storage::all();
        return $storages;
    }
}
